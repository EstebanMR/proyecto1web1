<?php
    include_once "head.php";
    include_once "../objetos/categorias.php";
    include_once "../objetos/productos.php";
    include_once "../objetos/sesionusuario.php";
    include_once "../objetos/usuario.php";
    include_once "../objetos/carrito.php";

    /* inicia el usuario y la sesion */
    $usuario = new Usuario();
    $sesion = new usuarioSesion();
    $sesion->_constructor();

    $usuario = $sesion->darUsuarioActual();

    $total=0;
    $carritos = array();
    $init=new carrito();
    $conectar= $init->conect();
    if($conectar){
        $script = "SELECT `id`, `usuario`, `fecha`, `producto`, `cantidad`, `descripcion`, `precio` FROM `carrito` WHERE usuario=".$usuario->id;;
                                      
        try{
            $ejecucion=mysqli_query($conectar, $script);
            $respu = $ejecucion->fetch_all();

            foreach ($respu as $res) {
                $car=new carrito();
                $car->id = $res[0];
                $car->usuario = $res[1];
                $car->fecha = $res[2];
                $car->producto = $res[3];
                $car->cantidad = $res[4];
                $car->descripcion = $res[5];
                $car->precio = $res[6];
                $carritos[]=$car;
                $total +=$res[6];
            }
                                            
        }catch(Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        } 
    }    
    //var_dump($carritos);
?>
<body style="width:100%; height:100%; overflow:hidden"><!-- -->
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="inicio.php" class="brand-logo">ESHOP  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <!--<li><a href="sass.html">Sass</a></li>-->
                    <li><a href="carrito.php"><i class=" material-icons">shopping_cart</i></a></li>
                    <li><a href="compras.php">Historial de compras</a></li>
                    <li><a href="../salir.php" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
        </div>
        <div style="height:90vh" class="col s10 yellow container"  style="display:block; overflow-y:auto; height:90vh">
            <div class="col s1"></div>
                <div class="col s9">
                    <br>
                    <br>
                    <h4 style="margin:auto; text-align:center;">Carrito de compras</h4>
                    <br>
                    <br>
                    <table>
                        <thead>
                            <tr>
                                <th>producto</th>
                                <th>descripcion</th>
                                <th>cantidad</th>
                                <th>precio</th>
                                <th>eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($carritos as $key) {
                                    if($conectar){
                                        $sql = "SELECT `imagen` FROM `productos` WHERE id=".$key->producto;
    
                                        try{
                                            $ejecucion=mysqli_query($conectar, $sql);
                                            $res1 = $ejecucion->fetch_all();
    
                                            foreach ($res1 as $resp1) {
                                                $img =$resp1[0]; 
                                            }
                                            
                                        }catch(Exception $e) {
                                            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                                        } 
                                    } 
                                    echo '<tr>
                                            <td><img width="100vw" src="data:image/jpg;base64,'.base64_encode($img).'"/></td>
                                            <td>'.$key->descripcion.'</td>
                                            <td>'.$key->cantidad.'</td>
                                            <td>₡'.$key->precio.'</td>
                                            <td><a style="color:#ef6c00; text-decoration: none;" href="eliminar_carrito.php?id='.$key->id.'" >ELIMINAR</a></td>
                                        </tr>';
                                } 
                            ?> 
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>TOTAL</td>
                                <td><?php  echo "₡".$total; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <br>
                    <div style="margin:auto; display: flex; justify-content:flex-end;">
                        <a data-target="modal1" class="btn waves-effect waves-light large btn-primary orange darken-3 modal-trigger" href="#modal1">COMPRAR</a>
                    </div>
                    <div id="modal1" class="modal">
                        <div class="modal-content">
                                <h4>Checkout</h4>
                            <?php
                                include_once "revisar_stock.php";

                            ?>
                        </div>
                        <div class="modal-footer">
                            <a href="" class="modal-close waves-effect waves-green btn-flat">X</a>
                        </div>
                    </div>
                </div>
            <div class="col s2" style=" height:90vh">
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
    $(document).ready(function(){
    $('.modal').modal();
  });

</script>

