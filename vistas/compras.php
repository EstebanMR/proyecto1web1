<?php
    include_once "head.php";
    include_once "../objetos/categorias.php";
    include_once "../objetos/productos.php";
    include_once "../objetos/sesionusuario.php";
    include_once "../objetos/usuario.php";
    include_once "../objetos/carrito.php";

    
    if (isset($_GET["fecha"])) {
        $fecha1=$_GET["fecha"];
    }
  

    /* inicia el usuario y la sesion */
    $usuario = new Usuario();
    $sesion = new usuarioSesion();
    $sesion->_constructor();

    $usuario = $sesion->darUsuarioActual();

    $compras=array();
    $init=new carrito();
    $conectar= $init->conect();
    if($conectar){
        $script = "SELECT `id`, `fecha`, `precio` FROM `ventas` WHERE usuario=".$usuario->id;;
                                      
        try{
            $ejecucion=mysqli_query($conectar, $script);
            $respu = $ejecucion->fetch_all();

            foreach ($respu as $res) {
                $car=new carrito();
                $car->id = $res[0];
                $car->fecha = $res[1];
                $car->precio = $res[2];
                $carritos[]=$car;
            }
                                            
        }catch(Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        } 
    }

    $venta=array();
    $temp= $carritos[0]->fecha;
    $array=explode(" ", $temp);
    $fecha=$array[0];
    $cant=count($carritos);
    $n=0;
    $total=0;
    foreach ($carritos as $key) {
        $n++;
        $temp1= $key->fecha;
        $array1=explode(" ", $temp1);
        if ($n==$cant) {
            $total=$total+$key->precio;
            $venta[]=$fecha."?".$total;
        } else if ($fecha==$array1[0]) {
            $total=$total+$key->precio;
        } else if (!$fecha==$array1[0]) {
            $venta[]=$fecha."?".$total;
            $total=$key->precio;
            $fecha=$array1[0];
        }
        
    }   
?>
 <body style="width:100%; height:100%; overflow:hidden"> <!--  -->
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="inicio.php" class="brand-logo">ESHOP  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <!-- <li><a href="sass.html">Sass</a></li> -->
                    <li><a href="carrito.php"><i class=" material-icons">shopping_cart</i></a></li>
                    <li><a href="">Historial de compras</a></li>
                    <li><a href="../salir.php" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
        </div>
        <div style="height:90vh" class="col s10 yellow container"  style="display:block; overflow-y:auto; height:90vh">
            <div class="col s1"></div>
                <div class="col s8">
                    <br>
                    <br>
                    <h4 style="margin:auto; text-align:center;">Listado de compras</h4>
                    <br>
                    <br>
                    <table>
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Total</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($venta as $value) {
                                    $array1=explode("?", $value);
                                    echo '<tr>
                                        <td>'.$array1[0].'</td>
                                        <td>'.$array1[1].'</td>
                                        <td><a style="color:#ef6c00; text-decoration: none;" href="compras.php?fecha='.$array1[0].'" >VER</a></td>
                                    </tr>';
                                } 
                                
                            ?> 
                        </tbody>
                    </table>
                    <br>
                    <br>
                </div>
            <div class="col s3" style="display:block; overflow-y:auto; height:90vh">
                <br>
                <br>
                <h4 style="margin:auto; text-align:center;">Listado de compras</h4>
                <br>
                <br>
                <h5><?php if (isset($fecha1)) {
                    echo $fecha1;
                }?></h5>
                <table>
                    <thead>
                        <tr>
                            <th>Descripcion</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $total_pre=0;
                            if (isset($fecha1)) {
                                if($conectar){
                                    $sql = "SELECT `descripcion`, `cantidad`, `precio` FROM `ventas` WHERE usuario=".$usuario->id." AND fecha like '".$fecha1."%%'";
                                    
                                    try{
                                        $ejecucion=mysqli_query($conectar, $sql);
                                        $res1 = $ejecucion->fetch_all();

                                        foreach ($res1 as $resp1) {
                                            echo '<tr>
                                                    <td>'.$resp1[0].'</td>
                                                    <td>'.$resp1[1].'</td>
                                                    <td>'.$resp1[2].'</td>
                                                </tr>';
                                            $total_pre+=$resp1[2];
                                        }
                                        
                                    }catch(Exception $e) {
                                        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                                    } 
                                } 
                                
                                
                            }
                        ?>
                        <tr><td></td><td>Total</td><td><?php echo$total_pre;?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
