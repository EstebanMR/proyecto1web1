<?php
    include_once "../objetos/productos.php";
    if (isset($_POST['codigo'])) {
        $ruta_imagen=$_FILES['imagen']['tmp_name'];
        $nombre_imagen=$_FILES['imagen']['name'];
        $tamano_imagen=$_FILES['imagen']['size'];

        $ruta_destino=$_SERVER['DOCUMENT_ROOT']."/img/";

        if ($tamano_imagen<=1000000) {
            move_uploaded_file($ruta_imagen, $ruta_destino.$nombre_imagen);

            $fp = fopen($ruta_destino.$nombre_imagen, "r");
            $contenido = fread($fp, $tamano_imagen);
            $contenido = addslashes($contenido);
            fclose($fp);            
            
            $pro = new producto();
            $pro->codigo=$_POST['codigo'];
            $pro->nombre=$_POST['nombre1'];
            $pro->descripcion=$_POST['descripcion'];
            $pro->imagen=$contenido;
            $pro->categoria=$_POST['categoria'];
            $pro->stock=$_POST['stock'];
            $pro->precio=$_POST['precio'];
            if (isset($_POST['id'])) {
                $pro->id=$_POST['id'];
                $pro->actualizar();
            }else if (!isset($_POST['id'])) {
                $pro->crear();
            }
        } else {
            echo "La imagen es demasiado pesada";
            die();
        }
        
        
    }
?>