<?php
    include_once "vistas\head.php";
?>
<body>
    <div class="row" style="padding-top:1%; padding-bottom:1%">
        <div class="col s4"></div>
        <div class="col s4 valign-wrapper" style="height: 93vh; display: flex; flex-direction:column;">
            <br>
            <img style="border-radius: 150%; overflow: hidden;" src="\img\user-icon-orange.jpg" width="200" height="200">
            <br>
            <div style=" height:57vh; border-radius:50px; border: black 0.5px solid; padding:5%; ">
                <h4>Iniciar sesión</h4>
                <br>
                <div class="p-2" >
                    <div style="color: red;">
                        <?php
                            if(isset($errorLogin)){
                                echo $errorLogin;
                            }
                        ?>
                    </div>
                </div>
                <form  method="POST">
                    <div>
                        <label>Nombre de usuario</label>
                        <input type="text" name="nombre" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Nombre de usuario">
                    </div>
                    <div>
                        <label>Contraseña</label>
                        <input type="password" name="contra" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Contrasña">
                    </div>
                    <br>
                    <br>
                    <input type="submit" class="btn large btn-primary orange darken-3" value="Ingresar" name="enviar" style="width: 100%; border-radius:50px; color:black"></input>
                </form>
                <br>
                <a class="btn large btn-primary orange darken-3" style="width: 100%; border-radius:50px; color:black" href="vistas\registro.php">Registrarse</a>
            </div>
        </div>
        <div class="col s4"></div>
    </div>
</body>
</html>