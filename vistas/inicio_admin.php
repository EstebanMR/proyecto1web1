<?php
    include_once "head.php";
    include_once "../objetos/sesionusuario.php";
    include_once "../objetos/usuario.php";
    include_once "../objetos/categorias.php";
    include_once "../objetos/productos.php";

    
    $usuario = new Usuario();
    $sesion = new usuarioSesion();
    $sesion->_constructor();

    /* verifica y valida el usuario */

    $usuario = $sesion->darUsuarioActual();
    if ((isset($_SESSION['usuario'])&& $usuario->admin =="0")) {
        header('location: ../index');
    }else if (!isset($_SESSION['usuario'])) {
        header('location: ../index');
    }
?>

<body style="width:100%; height:100%;" >
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="inicio_admin.php" class="brand-logo">ESHOP <span style="font-size:small">DASHBOARD</span>  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <!--<li><a href="sass.html">Sass</a></li>
                    <li><a href="badges.html">Components</a></li>
                    <li><a href=""><i class="large material-icons">shopping_cart</i></a></li>-->
                    <li><a href="../salir.php" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
            <br>
            <br>
            <br>
            <div class="row">
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="crud_categorias.php">Categorias</a>
                </div>
                <br>
                <br>
                <br>
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="crud_productos.php">Productos</a>
                </div>
            </div>
        </div>
        <div style="height:90vh" class="col s10 yellow ">
            <div class="col s4 container" style="height:90vh; display: flex;align-items: center">
                <table class="centered responsive-table">
                    <thead>
                        <tr>
                            <th>
                                <h4>Cantidad de usuarios registrados</h4>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php
                                    $us= new Usuario();
                                    $cant=$us->cantidad_usuarios();
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col s4" style="height:90vh; display: flex;align-items: center">
            <table class="centered responsive-table">
                    <thead>
                        <tr>
                            <th>
                                <h4>Cantidad de productos vendidos</h4>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php
                                    $pro= new producto();
                                    $conexion=$pro->conect();
                                    if($conexion){
                                        $sql = "SELECT SUM(cantidad) FROM `ventas`";
    
                                        try{
                                            $ejecucion=mysqli_query($conexion, $sql);
                                            $res1 = $ejecucion->fetch_all();
                                            
                                            echo $res1[0][0];                                           
                                            
                                        }catch(Exception $e) {
                                            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                                        } 
                                    } 
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col s4" style="height:90vh; display: flex;align-items: center">
            <table class="centered responsive-table">
                    <thead>
                        <tr>
                            <th>
                                <h4>Monto total de ventas en colones</h4>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php
                                    if($conexion){
                                        $sql1 = "SELECT SUM(precio) FROM `ventas`";
    
                                        try{
                                            $ejecucion1=mysqli_query($conexion, $sql1);
                                            $res2 = $ejecucion1->fetch_all();
                                            
                                            echo "₡".$res2[0][0];                                           
                                            
                                        }catch(Exception $e) {
                                            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                                        } 
                                    } 
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.