<?php include_once "head.php";?>
<body>
    <div class="row" style="padding-top:1%; padding-bottom:1%">
        <div class="col s4"></div>
        <div class="col s4 valign-wrapper" style="height: 93vh; display: flex; flex-direction:column;">
            <br>
            <div style=" border-radius:50px; border: black 0.5px solid; padding:5%; ">
                <h6>Registro</h6>
                <br>
                <form action=" registrar.php" method="POST">

                        <label>Nombre</label>
                        <input type="text" name="nombre" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Nombre">
                        
                        <label>Apellidos</label>
                        <input type="text" name="apellidos" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Apellidos">
                        
                        <label>Número de teléfono</label>
                        <input type="text" name="tel" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Teléfono">
                        
                        <label>Correo</label>
                        <input type="text" name="correo" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Correo">
                        
                        <label>Dirección</label>
                        <input type="text" name="dir" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Dirección">
                        
                        <label>Nombre de usuario</label>
                        <input type="text" name="nombreu" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Nombre de usuario">

                        <label>Contraseña</label>
                        <input type="password" name="contra" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Contrasña">
                    <br>
                    <br>
                    <input type="submit" class="btn large btn-primary orange darken-3" value="Ingresar" name="enviar" style="width: 100%; border-radius:50px; color:black"></input>
                </form>
            </div>
            <br>
            <br>
        </div>
        <div class="col s4"></div>
    </div>
</body>
</html>
