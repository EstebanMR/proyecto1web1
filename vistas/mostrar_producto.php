<?php
    include_once "head.php";
    include_once "../objetos/categorias.php";
    include_once "../objetos/productos.php";
    include_once "../objetos/sesionusuario.php";
    include_once "../objetos/usuario.php";
    include_once "../objetos/carrito.php";

    /* inicia el usuario y la sesion */
    $usuario = new Usuario();
    $sesion = new usuarioSesion();
    $sesion->_constructor();

    $usuario = $sesion->darUsuarioActual();

    /* toma el id del producto enviado por url */
    if (isset($_GET["id"])) {
        $id=$_GET["id"];
    }

    /* busca los datos del producto */
    $pro = new producto();
    $conectar= $pro->conect();
    if($conectar){
        $script = "SELECT `id`, `codigo`, `nombre`, `descripcion`, `imagen`, `categoria`, `stock`, `precio` FROM `productos`WHERE id=".$id;
                                      
        try{
            $ejecucion=mysqli_query($conectar, $script);
            $res = $ejecucion->fetch_all();

            $pro = new producto();
            $pro->id = $res[0][0];
            $pro->codigo = $res[0][1];
            $pro->nombre = $res[0][2];
            $pro->descripcion = $res[0][3];
            $pro->imagen = $res[0][4];
            $pro->categoria = $res[0][5];
            $pro->stock = $res[0][6];
            $pro->precio = $res[0][7]; 
                                            
        }catch(Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        } 
    }    
?>
<body style="width:100%; height:100%; "><!-- overflow:hidden -->
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="inicio.php" class="brand-logo">ESHOP  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <!--<li><a href="sass.html">Sass</a></li>-->
                    <li><a href="carrito.php"><i class=" material-icons">shopping_cart</i></a></li>
                    <li><a href="compras.php">Historial de compras</a></li>
                    <li><a href="../salir.php" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
        </div>
        <div style="height:90vh" class="col s10 yellow"  style="display:block; overflow-y:auto; height:90vh">
            <div class="col s1"></div>
            <div class="col s9">
                <div style="height:10vh">
                </div>
                <div style="display: flex;justify-content:center;">
                    <?php 
                    /* imprime la imagen del producto */
                        echo '<img width="250vw" src="data:image/jpg;base64,'.base64_encode($pro->imagen).'"/>';
                    ?>
                </div>
                <div>
                    <h4><?php echo $pro->nombre?></h4>
                    <h6>SKU:<?php echo $pro->codigo?></h6>
                    <br>
                    <h6>Descripción:</h6>
                    <p><?php echo $pro->descripcion?></p>
                </div>
            </div>
            <div class="col s2" style=" height:90vh">
                <br>
                <br>
                <br>
                <form action="" method="POST">
                    <label for="">Stock</label>
                    <input disabled type="number" name="stock" value="<?php echo $pro->stock;?>">
                    <input style="visibility:hidden" name="id" type="text" value="<?php echo $pro->id;?>">
                    <label disabled for="">Precio en ₡</label>
                    <input disabled type="number" name="precio" value="<?php echo $pro->precio; ?>">
                    <label for="">Cantidad:</label>
                    <input required type="number" name="cant" value="1">
                    <br>
                    <input style="font-size:1vw" type="submit" class="btn waves-effect waves-light large btn-primary orange darken-3" name="aceptar" value="Agregar al carrito">
                </form>
            </div>
        </div>
    </div>
</body>
</html>


<?php
    if (isset($_POST['aceptar'])) {
        $carrito=new carrito();
        $carrito->usuario=$usuario->id;
        $time = time();
        $fecha=date("d/m/Y H", $time);
        $carrito->fecha=$fecha;
        $carrito->producto=$id;
        $carrito->cantidad= $_POST['cant'];
        $carrito->descripcion=$pro->descripcion;
        $carrito->precio=$_POST['cant']*$pro->precio;
        echo"llego";

        $carrito->crear();
    }
?>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
