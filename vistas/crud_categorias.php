<?php

    if (isset($_GET["id"])) {
        $id=$_GET["id"];
        $nombre=$_GET["name"];
    }

    include_once "head.php";
    include_once "../objetos/sesionusuario.php";
    include_once "../objetos/usuario.php";
    include_once "../objetos/categorias.php";

    
    $usuario = new Usuario();
    $sesion = new usuarioSesion();
    $sesion->_constructor();

    /* verifica y valida el usuario */

    $usuario = $sesion->darUsuarioActual();
    if ((isset($_SESSION['usuario'])&& $usuario->admin =="0")) {
        header('location: ../index');
    }else if (!isset($_SESSION['usuario'])) {
        header('location: ../index');
    }

    $categorias = array();

?>
<body style="width:100%; height:100%; overflow:hidden" >
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="inicio_admin.php" class="brand-logo">ESHOP <span style="font-size:small">DASHBOARD</span>  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <!--<li><a href="sass.html">Sass</a></li>
                    <li><a href="badges.html">Components</a></li>
                    <li><a href=""><i class="large material-icons">shopping_cart</i></a></li>-->
                    <li><a href="../salir.php" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
            <br>
            <br>
            <br>
            <div class="row">
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="crud_categorias.php">Categorias</a>
                </div>
                <br>
                <br>
                <br>
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="crud_productos.php">Productos</a>
                </div>
            </div>
        </div>
        <div style="height:90vh" class="col s10 yellow ">
            <div class="col s6" style="height:90vh; display:block; overflow-y:auto; height:85vh; width:80vh;">
                <div class="container" >
                    <h3 style="margin:auto; text-align:center;">Listado de productos</h3>
                    <br>
                    <table class="centered responsive-table">
                        <thead>
                            <tr>
                                <th>ID  </th>
                                <th>Categorías</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>    
                        <tbody>
                            <?php
                                $init = new categoria();
                                $conectar= $init->conect();
                                if($conectar){
                                    $script = "SELECT `id`, `nombre` FROM categorias";
                                    
                                    try{
                                        $ejecucion=mysqli_query($conectar, $script);
                                        $res = $ejecucion->fetch_all();
                        
                                        foreach ($res as $categoria) {
                                            $cat = new categoria();
                                            $cat->id = $categoria[0];
                                            $cat->nombre = $categoria[1];
                                            $categorias[] = $cat;
                                        } 
                                        
                                    }catch(Exception $e) {
                                        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                                    }
                                }

                                foreach ($categorias as $value) {
                                    echo '<tr>
                                            <td>'.$value->id.'</td>
                                            <td>'.$value->nombre.'</td>
                                            <td><a style="color:#ef6c00; text-decoration: none;" href="eliminar_cat.php?id='.$value->id.'&name='.$value->nombre.'" >ELIMINAR</a>  |  <a style="color:#ef6c00; text-decoration: none;" href="crud_categorias.php?id='.$value->id.'&name='.$value->nombre.'" >EDITAR</a> </td>
                                        </tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col s6" style=" height:90vh">
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="container">
                    <h5>Categorías</h5>
                    <br>
                    <br>
                    <div>
                            <?php

                                if (isset($id)) {
                                    echo '<form action="editar_cat.php" method="POST">
                                    <input style="visibility:hidden" name="id" type="text" value="'.$id.'">
                                    <label for="name">Nombre de la categoría</label>
                                    <input style="background-color: #ffeb3b" name="nombre1" id="nombre" type="text" value="'.$nombre.'">';
                                }else{
                                    echo '<form action="editar_cat.php" method="POST"><label for="name">Nombre de la categoría</label><input style="background-color: #ffeb3b" name="nombre1" id="nombre" type="text">';
                                }
                            ?>
                            <br>
                            <br>
                            <br>
                            <input style="width: 100%;" class="btn large btn-primary orange darken-3" type="submit" value="Guardar" name="guardar">
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.