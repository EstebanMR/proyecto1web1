<?php

    if (isset($_GET["id"])) {
        $id=$_GET["id"];
    }

    include_once "head.php";
    include_once "../objetos/sesionusuario.php";
    include_once "../objetos/usuario.php";
    include_once "../objetos/productos.php";
    include_once "../objetos/categorias.php";

    
    $usuario = new Usuario();
    $sesion = new usuarioSesion();
    $sesion->_constructor();

    /* verifica y valida el usuario */

    $usuario = $sesion->darUsuarioActual();
    if ((isset($_SESSION['usuario'])&& $usuario->admin =="0")) {
        header('location: ../index');
    }else if (!isset($_SESSION['usuario'])) {
        header('location: ../index');
    }

    $categorias = array();
    $init= new categoria();
    $conectar=$init->conect();
        if($conectar){
            $script = "SELECT `id`, `nombre` FROM categorias";
            $categorias = array();
            try{
                $ejecucion=mysqli_query($conectar, $script);
                $res = $ejecucion->fetch_all();

                foreach ($res as $categoria) {
                    $cat = new categoria();
                    $cat->id = $categoria[0];
                    $cat->nombre = $categoria[1];
                    $categorias[] = $cat;
                } 
                
            }catch(Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }

    $productos = array();
?>
<body style="width:100%; height:100%; overflow:hidden" >
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="inicio_admin.php" class="brand-logo">ESHOP <span style="font-size:small">DASHBOARD</span>  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <!--<li><a href="sass.html">Sass</a></li>
                    <li><a href="badges.html">Components</a></li>
                    <li><a href=""><i class="large material-icons">shopping_cart</i></a></li>-->
                    <li><a href="../salir.php" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
            <br>
            <br>
            <br>
            <div class="row">
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="crud_categorias.php">Categorias</a>
                </div>
                <br>
                <br>
                <br>
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="crud_productos.php">Productos</a>
                </div>
            </div>
        </div>
        <div style="height:90vh" class="col s10 yellow ">
            <div class="col s8">
                <div style="display:block; overflow-y:auto; height:90vh">
                    <h3 style="margin:auto; text-align:center;">Listado de productos</h3>
                    <br>
                    <table class=" centered responsive-table">
                        <thead>
                            <tr>
                                <th>ID  </th>
                                <th>SKU</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Imagen</th>
                                <th>Categoria</th>
                                <th>Stock</th>
                                <th>Precio</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>    
                        <tbody>
                            <?php
                                $init = new producto();
                                $conectar= $init->conect();
                                if($conectar){
                                    $script = "SELECT `id`, `codigo`, `nombre`, `descripcion`, `imagen`, `categoria`, `stock`, `precio` FROM `productos`";
                                    
                                    try{
                                        $ejecucion=mysqli_query($conectar, $script);
                                        $res = $ejecucion->fetch_all();
                        
                                        foreach ($res as $prod) {
                                            $pro = new producto();
                                            $pro->id = $prod[0];
                                            $pro->codigo = $prod[1];
                                            $pro->nombre = $prod[2];
                                            $pro->descripcion = $prod[3];
                                            $pro->imagen = $prod[4];
                                            $pro->categoria = $prod[5];
                                            $pro->stock = $prod[6];
                                            $pro->precio = $prod[7];
                                            $productos[] = $pro;
                                        } 
                                        
                                    }catch(Exception $e) {
                                        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                                    }
                                }

                                foreach ($productos as $value) {
                                    echo '<tr>
                                            <td>'.$value->id.'</td>
                                            <td>'.$value->codigo.'</td>
                                            <td>'.$value->nombre.'</td>
                                            <td>'.$value->descripcion.'</td>
                                            <td><img width="100vw" src="data:image/jpg;base64,'.base64_encode($value->imagen).'"/></td>
                                            <td>'.$value->categoria.'</td>
                                            <td>'.$value->stock.'</td>
                                            <td>'.$value->precio.'</td>
                                            <td><a style="color:#ef6c00; text-decoration: none;" href="eliminar_pro.php?id='.$value->id.'" >ELIMINAR</a>  |  <a style="color:#ef6c00; text-decoration: none;" href="crud_productos.php?id='.$value->id.'" >EDITAR</a> </td>
                                        </tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col s4" style=" height:90vh">
                <div class="container" style="display:block; overflow-y:auto; height:90vh; width:25VW">
                    <h5>Productos</h5>
                    <div>
                            <?php
                                if (isset($id)) {
                                    $pr = new producto();
                                    foreach ($productos as $prod) {
                                        if ($prod->id==$id) {
                                            $pr->id = $prod->id;
                                            $pr->codigo = $prod->codigo;
                                            $pr->nombre = $prod->nombre;
                                            $pr->descripcion = $prod->descripcion;
                                            $pr->imagen = $prod->imagen;
                                            $pr->categoria = $prod->categoria;
                                            $pr->stock = $prod->stock;
                                            $pr->precio = $prod->precio;
                                        }                                                                                
                                    } 

                                    echo '<form action="editar_pro.php" method="POST" enctype="multipart/form-data">
                                    <input style="visibility:hidden" name="id" type="text" value="'.$pr->id.'">
                                    <label >Codigo del producto</label>
                                    <input style="background-color: #ffeb3b" name="codigo" type="text" value="'.$pr->codigo.'">
                                    <label >Nombre del producto</label>
                                    <input style="background-color: #ffeb3b" name="nombre1" type="text" value="'.$pr->nombre.'">
                                    <label >Descripcion del producto</label>
                                    <input style="background-color: #ffeb3b" name="descripcion"  type="text" value="'.$pr->descripcion.'">
                                    <label >Imagen del producto</label>
                                    <br>
                                    <img width="170vw" src="data:image/jpg;base64,'.base64_encode($pr->imagen).'"/>
                                    <br>
                                    <input style="background-color: #ffeb3b" name="imagen" type="file" REQUIRED>
                                    <br>
                                    <label >Categoria del producto</label>
                                    <select name="categoria" id="categoria" style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge">
                                        <option value="" style="border-bottom: 1px solid #ef6c00; box-shadow: 0 1px 0 0 #ef6c00"><span style="color:#ef6c00">Marque una categoría</span></option>';
                                        
                                        foreach ($categorias as $valor) {
                                            if ($valor->id==$pr->id) {
                                                echo '<option selected style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge" value="'.$valor->id.'"><span style="color:#ef6c00">'.$valor->nombre.'</span></option>';
                                            } else {
                                                echo '<option style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge" value="'.$valor->id.'"><span style="color:#ef6c00">'.$valor->nombre.'</span></option>';
                                            }
                                        }
                                         
                                    echo '</select><br>
                                    <label >Stock del producto</label>
                                    <input style="background-color: #ffeb3b" name="stock" type="text" value="'.$pr->stock.'">
                                    <label >Precio del producto</label>
                                    <input style="background-color: #ffeb3b" name="precio" type="text" value="'.$pr->precio.'">';
                                }else{
                                    echo '<form action="editar_pro.php" method="POST" enctype="multipart/form-data">
                                    <label >Codigo del producto</label>
                                    <input style="background-color: #ffeb3b" name="codigo" type="text" value="">
                                    <label >Nombre del producto</label>
                                    <input style="background-color: #ffeb3b" name="nombre1" type="text" value="">
                                    <label >Descripcion del producto</label>
                                    <input style="background-color: #ffeb3b" name="descripcion"  type="text" value="">
                                    <label >Imagen del producto</label>
                                    <br>
                                    <img width="170vw" src="https://www.pequenomundo.cl/wp-content/themes/childcare/images/default.png"/>
                                    <br>
                                    <input style="background-color: #ffeb3b" name="imagen" type="file" REQUIRED+>
                                    <br>
                                    <label >Categoria del producto</label>
                                    <select name="categoria" id="categoria" style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge">
                                        <option value="" style="border-bottom: 1px solid #ef6c00; box-shadow: 0 1px 0 0 #ef6c00"><span style="color:#ef6c00">Marque una categoría</span></option>';
                                        
                                        foreach ($categorias as $valor) {
                                            echo '<option style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge" value="'.$valor->id.'"><span style="color:#ef6c00">'.$valor->nombre.'</span></option>';
                                        }
                                         
                                    echo '</select><br>
                                    <label >Stock del producto</label>
                                    <input style="background-color: #ffeb3b" name="stock" type="text" value="">
                                    <label >Precio del producto</label>
                                    <input style="background-color: #ffeb3b" name="precio" type="text" value="">';
                                }
                            ?>
                            <br>
                            <br>
                            <input style="width: 100%;" class="btn large btn-primary orange darken-3" type="submit" value="Guardar" name="guardar">
                        </form>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
    /* select */
    $(document).ready(function(){
        $('select').formSelect();
    });

</script>