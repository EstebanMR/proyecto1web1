<?php
    include_once "head.php";
    include_once "../objetos/sesionusuario.php";
    include_once "../objetos/usuario.php";
    include_once "../objetos/categorias.php";
    include_once "../objetos/productos.php";

    
    $usuario = new Usuario();
    $sesion = new usuarioSesion();
    $sesion->_constructor();

    /* verifica y valida el usuario */

    $usuario = $sesion->darUsuarioActual();
    if (isset($_SESSION['usuario'])&& $usuario->admin =="0") {

    }else if ((isset($_SESSION['usuario'])&& $usuario->admin =="1")) {
        header('location: ../index');
    }else if (!isset($_SESSION['usuario'])) {
        header('location: ../index');
    }

    /* trae las categorias de la base de datos */
    $categorias = array();
    $servidor="localhost";
    $usuario="root";
    $clave="";
    $db="proyecto";
    try{
        $conectar=mysqli_connect($servidor,$usuario,$clave,$db);
        if($conectar){
            $script = "SELECT `id`, `nombre` FROM categorias";
            
            try{
                $ejecucion=mysqli_query($conectar, $script);
                $res = $ejecucion->fetch_all();

                foreach ($res as $categoria) {
                    $cat = new categoria();
                    $cat->id = $categoria[0];
                    $cat->nombre = $categoria[1];
                    $categorias[] = $cat;
                } 
                
            }catch(Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }
    } catch(Exception $error){
        print_r('Error connection: ' . $e->getMessage());
    }

    $productos = array();
    $init = new producto();
    $conectar= $init->conect();
    if($conectar){
        $script = "SELECT `id`, `codigo`, `nombre`, `descripcion`, `imagen`, `categoria`, `stock`, `precio` FROM `productos`";
                                        
        try{
            $ejecucion=mysqli_query($conectar, $script);
            $res = $ejecucion->fetch_all();
                                
            foreach ($res as $prod) {
                $pro = new producto();
                $pro->id = $prod[0];
                $pro->codigo = $prod[1];
                $pro->nombre = $prod[2];
                $pro->descripcion = $prod[3];
                $pro->imagen = $prod[4];
                $pro->categoria = $prod[5];
                $pro->stock = $prod[6];
                $pro->precio = $prod[7];
                $productos[] = $pro;
            } 
                                            
        }catch(Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
?>

<body style="width:100%; height:100%; overflow:hidden" >
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="inicio.php" class="brand-logo">ESHOP  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <!--<li><a href="sass.html">Sass</a></li>-->
                    <li><a href="carrito.php"><i class=" material-icons">shopping_cart</i></a></li>
                    <li><a href="compras.php">Historial de compras</a></li>
                    <li><a href="../salir.php" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
            <br>
            <br>
            <div class="row">
                <form method="POST">
                    <div class="input-field col s9">
                        <select name="categorias" style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge">
                            <option value="all" style="border-bottom: 1px solid #ef6c00; box-shadow: 0 1px 0 0 #ef6c00"><span style="color:#ef6c00">Marque una categoría</span></option>
                            <?php
                                foreach ($categorias as $valor) {
                                    echo '<option style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge" value="'.$valor->id.'"><span style="color:#ef6c00">'.$valor->nombre.'</span></option>';
                                }
                            ?> 
                        </select>
                        <label>Categorías</label>
                    </div>
                    <div class="col s2"> 
                        <br>
                        <input type="submit" class="btn waves-effect waves-light orange darken-3" name="cat" style="width:3vw" value="🔍">
                    </div>  
                </form>
            </div>
            <table class="responsive-table centered">
                <thead>
                    <tr>
                        <th>Estadísticas</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            Total de productos
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <?php 
                        $usuario1 = $sesion->darUsuarioActual();
                        if($conectar){
                            
                            $script = "SELECT SUM(cantidad) FROM `ventas` WHERE usuario = ".$usuario1->id;
                                                            
                            try{
                                $ejecucion=mysqli_query($conectar, $script);
                                $res = $ejecucion->fetch_all();
                                
                                    echo $res[0][0];                     
                                                                
                            }catch(Exception $e) {
                                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                            }
                        }?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        Monto comprado
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ₡<?php 
                            if($conectar){
                                $script = "SELECT SUM(precio) FROM `ventas` WHERE usuario=".$usuario1->id;
                                                                
                                try{
                                    $ejecucion=mysqli_query($conectar, $script);
                                    $res = $ejecucion->fetch_all();
                                    
                                        echo $res[0][0];                     
                                                                    
                                }catch(Exception $e) {
                                    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                                }
                            }?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="display:block; overflow-y:auto; height:90vh" class="col s10 yellow">
            <div class="container" >
                <?php
                    if (isset($_POST['cat'])) {
                        $cate=$_POST['categorias'];
                        foreach ($categorias as $val) {
                            if ($val->id == $cate) {
                                echo '<h5>Categoría:" '.$val->nombre.' "</h5>';
                            }
                        }
                        echo '<table class=" responsive-table">
                                <tbody>';
                        $num1=0;
                        foreach ($productos as $key) {
                            if ($key->categoria==$cate) {
                                if ($num1==0) {
                                    echo '<tr>
                                            <td>
                                                <div class="container">
                                                    <table class="centered responsive-table">
                                                        <tbody>
                                                            <tr style="height:20vh">
                                                                <td>
                                                                    <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key->imagen).'"/>   
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <span>'.$key->nombre.'</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a class="waves-effect waves-light btn modal-trigger col s12" href="mostrar_producto.php?id='.$key->id.'">Ver info.</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>';
                                }elseif ($num1==1) {
                                    echo '<td>
                                            <div class="container">
                                                <table class="centered responsive-table">
                                                    <tbody>
                                                        <tr style="height:20vh">
                                                            <td>
                                                                <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key->imagen).'"/>   
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>'.$key->nombre.'</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a class="waves-effect waves-light btn modal-trigger col s12" href="mostrar_producto.php?id='.$key->id.'">Ver info.</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>';
                                }elseif ($num1==2) {
                                    echo '<td>
                                            <div class="container">
                                                <table class="centered responsive-table">
                                                    <tbody>
                                                        <tr style="height:20vh">
                                                            <td>
                                                                <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key->imagen).'"/>   
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td>
                                                                <span>'.$key->nombre.'</span>
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td>
                                                                <a class="waves-effect waves-light btn modal-trigger col s12" href="mostrar_producto.php?id='.$key->id.'">Ver info.</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <tr>';
                                    $num=0;
                                }
                                $num1++;
                            }
                        }
                        echo '</tbody>
                        </table>';
                                    
                    }else if(!isset($_POST['cat'])){
                        echo '<h5>Todos los productos</h5>';

                        echo '<table class=" responsive-table">
                                <tbody>';
                        $num1=0;
                        foreach ($productos as $key) {
                            $num1++;
                                if ($num1==1) {
                                    echo '<tr>
                                            <td>
                                                <div class="container">
                                                    <table class="centered responsive-table">
                                                        <tbody>
                                                            <tr style="height:20vh">
                                                                <td>
                                                                    <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key->imagen).'"/>   
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <span>'.$key->nombre.'</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a class="waves-effect waves-light btn modal-trigger col s12" href="mostrar_producto.php?id='.$key->id.'">Ver info.</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>';
                                }elseif ($num1==2) {
                                    echo '<td>
                                            <div class="container">
                                                <table class="centered responsive-table">
                                                    <tbody>
                                                        <tr style="height:20vh">
                                                            <td>
                                                                <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key->imagen).'"/>   
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>'.$key->nombre.'</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a class="waves-effect waves-light btn modal-trigger col s12" href="mostrar_producto.php?id='.$key->id.'">Ver info.</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>';
                                }elseif ($num1==3) {
                                    echo '<td>
                                            <div class="container">
                                                <table class="centered responsive-table">
                                                    <tbody>
                                                        <tr style="height:20vh">
                                                            <td>
                                                                <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key->imagen).'"/>   
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td>
                                                                <span>'.$key->nombre.'</span>
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td>
                                                                <a class="waves-effect waves-light btn modal-trigger col s12" href="mostrar_producto.php?id='.$key->id.'">Ver info.</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <tr>';
                                    $num1=0;
                                }
                                
                        }
                        echo '</tbody>
                        </table>';
                    }
                ?>
            </div>
        </div>
    </div>
</body>
</html>



<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
    /* select */
    $(document).ready(function(){
        $('select').formSelect();
    });

</script>
