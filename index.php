<?php
    include_once "objetos/usuario.php";
    include_once "objetos/sesionusuario.php";

    /* inicializa los objetos necesarios */
    $sesion = new usuarioSesion();
    $usuario = new Usuario();

    /* inicia la sesion */
    $sesion->_constructor();

    if (isset($_SESSION['usuario'])) {

        /* si existe la sesion toma los datos y lo redirige a la pagina correspondiente */

        $array = $sesion->darUsuarioActual();
        $usuario->id = $array->id;
        $usuario->nombre = $array->nombre;
        $usuario->apellido = $array->apellido;
        $usuario->telefono = $array->telefono;
        $usuario->correo = $array->correo;
        $usuario->direccion = $array->direccion;
        $usuario->contrasenna = $array->contrasenna;
        $usuario->admin = $array->admin;
        $usuario->nusuario = $array->nusuario;
        $usuario->us = $array;

        if ($usuario->admin == "1") {
            header('location: vistas/inicio_admin.php');
            /* echo "es admin"; */
        }else if ($usuario->admin == "0"){
            header('location: vistas/inicio.php');
            //echo "no es admin";
        }

    }else if (isset($_POST['nombre']) && isset($_POST['contra'])) {

        /*  aqui se valida el login y se redirige a donde se requiera */
        $nu = $_POST['nombre'];
        $pas = $_POST['contra'];
        if($usuario->userExist($nu, $pas)){
            try{
                $usuario->setUsuario($nu, $pas);
                $sesion->cambiarUsuarioActual($usuario);

                if($usuario->admin==1){
                    header('location: vistas/inicio_admin.php');
                    /* echo "es admin"; */
                }else if ($usuario->admin==0) {
                    header('location: vistas/inicio.php');
                    //echo "no es admin";
                }
            } catch(Exception $error){
                print_r('Error: ' . $e->getMessage());
            }

        }else{
            $errorLogin="Datos incorrectos, intentelo de nuevo";
            include_once 'vistas/head.php';
            include_once 'vistas/login.php';
        }
    }else if(!isset($_SESSION['usuario'])){

        /* si no hay sesion manda el login */

        include_once 'vistas/head.php';
        include_once 'vistas/login.php';
    }
?>