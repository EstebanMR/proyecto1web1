<?php
    class usuarioSesion{

        /* inicializa la sesión */
        public function _constructor()
        {
            session_start();
        }

        /* agrega los datos del usuario a la sesión */
        public function cambiarUsuarioActual($usuario)
        {
            $_SESSION['usuario'] = $usuario;
        }

        /* retorna el usuario de la sesión */
        public function darUsuarioActual()
        {
            return $_SESSION['usuario'];
        }

        /* cierra sesión */
        public function cerrarSesion()
        {
            session_unset();
            session_destroy();
            header('location: ../index');
            
        }
    }
?>